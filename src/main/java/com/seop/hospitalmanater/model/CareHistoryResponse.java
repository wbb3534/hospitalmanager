package com.seop.hospitalmanater.model;

import com.seop.hospitalmanater.entity.CareHistory;
import com.seop.hospitalmanater.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class CareHistoryResponse {
    private Long customerId;
    private Long historyId;
    private String customerName;
    private String customerPhone;
    private String customerResidentNum;
    private String customerAddress;
    private String memo;
    private LocalDate visitFirst;
    private String medicalItem;
    private Double price;
    private Double nonSalary;
    private String isSalary;
    private LocalDate cureDate;
    private LocalTime cureTime;
    private String isCalculate;

    private CareHistoryResponse(CareHistoryResponseBuilder builder) {
        this.customerId = builder.customerId;
        this.historyId = builder.historyId;;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;;
        this.customerResidentNum = builder.customerResidentNum;
        this.customerAddress = builder.customerAddress;
        this.memo = builder.memo;
        this.visitFirst = builder.visitFirst;
        this.medicalItem = builder.medicalItem;
        this.price = builder.price;
        this.nonSalary = builder.nonSalary;
        this.isSalary = builder.isSalary;
        this.cureDate = builder.cureDate;
        this.cureTime = builder.cureTime;
        this.isCalculate = builder.isCalculate;
    }
    public static class CareHistoryResponseBuilder implements CommonModelBuilder<CareHistoryResponse> {
        private final Long customerId;
        private final Long historyId;
        private final String customerName;
        private final String customerPhone;
        private final String customerResidentNum;
        private final String customerAddress;
        private final String memo;
        private final LocalDate visitFirst;
        private final String medicalItem;
        private final Double price;
        private final Double nonSalary;
        private final String isSalary;
        private final LocalDate cureDate;
        private final LocalTime cureTime;
        private final String isCalculate;

        public CareHistoryResponseBuilder(CareHistory careHistory) {
            this.customerId = careHistory.getCustomerInfo().getCustomerId();
            this.historyId = careHistory.getHistoryId();
            this.customerName = careHistory.getCustomerInfo().getCustomerName();
            this.customerPhone = careHistory.getCustomerInfo().getCustomerPhone();
            this.customerResidentNum = careHistory.getCustomerInfo().getCustomerResidentNum();
            this.customerAddress = careHistory.getCustomerInfo().getCustomerAddress();
            this.memo = careHistory.getCustomerInfo().getMemo();
            this.visitFirst = careHistory.getCustomerInfo().getVisitFirst();
            this.medicalItem = careHistory.getMedicalItem().getTreatmentName();
            this.price = careHistory.getPrice();
            this.nonSalary = careHistory.getMedicalItem().getNonSalaryPrice();
            this.isSalary = careHistory.getIsSalary() ? "Y" : "N";
            this.cureDate = careHistory.getCureDate();
            this.cureTime = careHistory.getCureTime();
            this.isCalculate = careHistory.getIsCalculate() ? "Y" : "N";
        }
        @Override
        public CareHistoryResponse build() {
            return new CareHistoryResponse(this);
        }
    }
}
