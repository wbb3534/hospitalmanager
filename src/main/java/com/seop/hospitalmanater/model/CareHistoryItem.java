package com.seop.hospitalmanater.model;

import com.seop.hospitalmanater.entity.CareHistory;
import com.seop.hospitalmanater.enums.MedicalItem;
import com.seop.hospitalmanater.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CareHistoryItem {
    private Long customerId;
    private Long historyId;
    private String customerName;
    private String customerPhone;
    private String customerResidentNum;
    private String medicalItem;
    private Double price;
    private String isSalary;
    private LocalDate cureDate;
    private LocalTime cureTime;
    private String isCalculate;

    private CareHistoryItem(CareHistoryItemBuilder builder) {
        this.customerId = builder.customerId;
        this.historyId = builder.historyId;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.customerResidentNum = builder.customerResidentNum;
        this.medicalItem = builder.medicalItem;
        this.price = builder.price;
        this.isSalary = builder.isSalary;
        this.cureDate = builder.cureDate;
        this.cureTime = builder.cureTime;
        this.isCalculate = builder.isCalculate;
    }
    public static class CareHistoryItemBuilder implements CommonModelBuilder<CareHistoryItem> {
        private final Long customerId;
        private final Long historyId;
        private final String customerName;
        private final String customerPhone;
        private final String customerResidentNum;
        private final String medicalItem;
        private final Double price;
        private final String isSalary;
        private final LocalDate cureDate;
        private final LocalTime cureTime;
        private final String isCalculate;

        public CareHistoryItemBuilder(CareHistory careHistory) {
            this.customerId = careHistory.getHistoryId();
            this.historyId = careHistory.getHistoryId();
            this.customerName = careHistory.getCustomerInfo().getCustomerName();
            this.customerPhone = careHistory.getCustomerInfo().getCustomerPhone();
            this.customerResidentNum = careHistory.getCustomerInfo().getCustomerResidentNum();
            this.medicalItem = careHistory.getMedicalItem().getTreatmentName();
            this.price = careHistory.getPrice();
            this.isSalary = careHistory.getIsSalary() ? "Y" : "N";
            this.cureDate = careHistory.getCureDate();
            this.cureTime = careHistory.getCureTime();
            this.isCalculate = careHistory.getIsCalculate() ? "Y" : "N";
        }
        @Override
        public CareHistoryItem build() {
            return new CareHistoryItem(this);
        }
    }
}
