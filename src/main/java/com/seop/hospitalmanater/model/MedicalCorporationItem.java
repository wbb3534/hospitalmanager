package com.seop.hospitalmanater.model;

import com.seop.hospitalmanater.entity.CareHistory;
import com.seop.hospitalmanater.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MedicalCorporationItem {
    private Long historyId;
    private String customerName;
    private String customerPhone;
    private String customerResidentNum;
    private String medicalItem;
    private Double price;
    private Double nonSalary;
    private Double claimsPrice;

    private MedicalCorporationItem(MedicalCorporationItemBuilder builder) {
        this.historyId = builder.historyId;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.customerResidentNum = builder.customerResidentNum;
        this.medicalItem = builder.medicalItem;
        this.price = builder.price;
        this.nonSalary = builder.nonSalary;
        this.claimsPrice = builder.claimsPrice;
    }
    public static class MedicalCorporationItemBuilder implements CommonModelBuilder<MedicalCorporationItem> {
        private final Long historyId;
        private final String customerName;
        private final String customerPhone;
        private final String customerResidentNum;
        private final String medicalItem;
        private final Double price;
        private final Double nonSalary;
        private final Double claimsPrice;

        public MedicalCorporationItemBuilder(CareHistory careHistory) {
            this.historyId = careHistory.getHistoryId();
            this.customerName = careHistory.getCustomerInfo().getCustomerName();
            this.customerPhone = careHistory.getCustomerInfo().getCustomerPhone();
            this.customerResidentNum = careHistory.getCustomerInfo().getCustomerResidentNum();
            this.medicalItem = careHistory.getMedicalItem().getTreatmentName();
            this.price = careHistory.getPrice();
            this.nonSalary = careHistory.getMedicalItem().getNonSalaryPrice();
            this.claimsPrice = careHistory.getMedicalItem().getNonSalaryPrice() - careHistory.getPrice();

        }
        @Override
        public MedicalCorporationItem build() {
            return new MedicalCorporationItem(this);
        }
    }
}
