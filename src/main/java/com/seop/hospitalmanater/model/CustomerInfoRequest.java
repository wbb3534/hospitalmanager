package com.seop.hospitalmanater.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerInfoRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 10, max = 13)
    private String customerPhone;
    @NotNull
    @Length(min = 10, max = 14)
    private String customerResidentNum;
    @NotNull
    @Length(min = 5, max = 100)
    private String customerAddress;
    @NotNull
    private String memo;
}
