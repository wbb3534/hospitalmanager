package com.seop.hospitalmanater.entity;

import com.seop.hospitalmanater.interfaces.CommonModelBuilder;
import com.seop.hospitalmanater.model.CustomerInfoRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long customerId;
    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 13)
    private String customerPhone;
    @Column(nullable = false, length = 14)
    private String customerResidentNum;
    @Column(nullable = false, length = 100)
    private String customerAddress;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String memo;
    @Column(nullable = false)
    private LocalDate visitFirst;

    private CustomerInfo(CustomerInfoBuilder builder) {
        this.customerName = builder.customerName;
        this.customerAddress = builder.customerAddress;
        this.customerPhone = builder.customerPhone;
        this.customerResidentNum = builder.customerResidentNum;
        this.memo = builder.memo;
        this.visitFirst = builder.visitFirst;
    }
    public static class CustomerInfoBuilder implements CommonModelBuilder<CustomerInfo> {
        private final String customerName;
        private final String customerPhone;
        private final String customerResidentNum;
        private final String customerAddress;
        private final String memo;
        private final LocalDate visitFirst;

        public CustomerInfoBuilder(CustomerInfoRequest request) {
            this.customerName = request.getCustomerName();
            this.customerPhone = request.getCustomerPhone();
            this.customerResidentNum = request.getCustomerResidentNum();
            this.customerAddress = request.getCustomerAddress();
            this.memo = request.getMemo();
            this.visitFirst = LocalDate.now();
        }

        @Override
        public CustomerInfo build() {
            return new CustomerInfo(this);
        }
    }
}
