package com.seop.hospitalmanater.entity;

import com.seop.hospitalmanater.enums.MedicalItem;
import com.seop.hospitalmanater.interfaces.CommonModelBuilder;
import com.seop.hospitalmanater.model.CareHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CareHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long historyId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerInfoId", nullable = false)
    private CustomerInfo customerInfo;
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private MedicalItem medicalItem;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private Boolean isSalary;
    @Column(nullable = false)
    private LocalDate cureDate;
    @Column(nullable = false)
    private LocalTime cureTime;
    @Column(nullable = false)
    private Boolean isCalculate;

    public void putCompletePay() {
        this.isCalculate = true;
    }

    private CareHistory(CareHistoryBuilder builder) {
        this.customerInfo = builder.customerInfo;
        this.medicalItem = builder.medicalItem;
        this.price = builder.price;
        this.isSalary = builder.isSalary;
        this.cureDate = builder.cureDate;
        this.cureTime = builder.cureTime;
        this.isCalculate = builder.isCalculate;
    }
    public static class CareHistoryBuilder implements CommonModelBuilder<CareHistory> {
        private final CustomerInfo customerInfo;
        private final MedicalItem medicalItem;
        private final Double price;
        private final Boolean isSalary;
        private final LocalDate cureDate;
        private final LocalTime cureTime;
        private final Boolean isCalculate;

        public CareHistoryBuilder(CustomerInfo customerInfo, CareHistoryRequest request) {
            this.customerInfo = customerInfo;
            this.medicalItem = request.getMedicalItem();
            this.price = request.getIsSalary() ? request.getMedicalItem().getSalaryPrice() : request.getMedicalItem().getNonSalaryPrice();
            this.isSalary = request.getIsSalary();
            this.cureDate = LocalDate.now();
            this.cureTime = LocalTime.now();
            this.isCalculate = false;
        }

        @Override
        public CareHistory build() {
            return new CareHistory(this);
        }
    }
}
