package com.seop.hospitalmanater.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
