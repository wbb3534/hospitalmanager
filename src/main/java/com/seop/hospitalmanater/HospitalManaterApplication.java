package com.seop.hospitalmanater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalManaterApplication {

    public static void main(String[] args) {
        SpringApplication.run(HospitalManaterApplication.class, args);
    }

}
