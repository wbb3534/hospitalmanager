package com.seop.hospitalmanater.repository;

import com.seop.hospitalmanater.entity.CareHistory;
import com.seop.hospitalmanater.model.CareHistoryItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface CareHistoryRepository extends JpaRepository<CareHistory, Long> {
    List<CareHistory> findAllByCureDateOrderByHistoryIdDesc(LocalDate searchDate);

    List<CareHistory> findAllByIsSalaryAndCureDateAndIsCalculateOrderByHistoryIdDesc(
            Boolean isSalary,
            LocalDate searchDate,
            Boolean isCalculate
            );
}
