package com.seop.hospitalmanater.repository;

import com.seop.hospitalmanater.entity.CustomerInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerInfoRepository extends JpaRepository<CustomerInfo, Long> {
}
