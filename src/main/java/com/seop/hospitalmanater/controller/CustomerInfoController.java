package com.seop.hospitalmanater.controller;

import com.seop.hospitalmanater.model.CustomerInfoRequest;
import com.seop.hospitalmanater.service.CustomerInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/customer-info")
@RequiredArgsConstructor
public class CustomerInfoController {
    public final CustomerInfoService customerInfoService;

    @PostMapping("/new")
    public String setCustomerInfo(@RequestBody @Valid CustomerInfoRequest request) {
        customerInfoService.setCustomerInfo(request);

        return "ok";
    }
}
