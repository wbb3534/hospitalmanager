package com.seop.hospitalmanater.controller;

import com.seop.hospitalmanater.entity.CustomerInfo;
import com.seop.hospitalmanater.model.CareHistoryItem;
import com.seop.hospitalmanater.model.CareHistoryRequest;
import com.seop.hospitalmanater.model.CareHistoryResponse;
import com.seop.hospitalmanater.model.MedicalCorporationItem;
import com.seop.hospitalmanater.service.CareHistoryService;
import com.seop.hospitalmanater.service.CustomerInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.cglib.core.Local;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/care-history")
public class CareHistoryController {
    private final CareHistoryService careHistoryService;
    private final CustomerInfoService customerInfoService;

    @PostMapping("/new/customer/{customerId}")
    public String setCareHistory(@PathVariable long customerId, @RequestBody @Valid CareHistoryRequest request) {
        CustomerInfo customerInfo = customerInfoService.getCustomerId(customerId);
        careHistoryService.setCareHistory(customerInfo, request);
        return "ok";
    }

    @GetMapping("/info/date")
    public List<CareHistoryItem> getCareHistoryByDate(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return careHistoryService.getHistoryByDate(searchDate);
    }

    @GetMapping("/info/medical")
    public List<MedicalCorporationItem> getMedicalCorporation(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return careHistoryService.getMedicalCorporation(searchDate);
    }

    @GetMapping("/detail/care-history/{historyId}")
    public CareHistoryResponse getCareHistory(@PathVariable long historyId) {
        return careHistoryService.getCareHistory(historyId);
    }

    @PutMapping("/calculate-completion/care-hisory/{historyId}")
    public String putCalculateCompletion(@PathVariable long historyId) {
        careHistoryService.putCalculate(historyId);

        return "ok";
    }
}
