package com.seop.hospitalmanater.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MedicalItem {
    CHUNA("추나 치료", 50000, 70000),
    PHYSICAL("물리 치료", 12000, 50000),
    ACUPUNCTURE("침 치료", 5000, 20000),
    CUPPING("부항", 7000, 25000)

    ;

    private final String treatmentName;
    private final double salaryPrice;
    private final double nonSalaryPrice;
}
