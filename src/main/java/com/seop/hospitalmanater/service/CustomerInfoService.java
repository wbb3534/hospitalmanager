package com.seop.hospitalmanater.service;

import com.seop.hospitalmanater.entity.CustomerInfo;
import com.seop.hospitalmanater.model.CustomerInfoRequest;
import com.seop.hospitalmanater.repository.CareHistoryRepository;
import com.seop.hospitalmanater.repository.CustomerInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class CustomerInfoService {
    private final CustomerInfoRepository customerInfoRepository;

    public void setCustomerInfo(CustomerInfoRequest request) {
        CustomerInfo addData = new CustomerInfo.CustomerInfoBuilder(request).build();

        customerInfoRepository.save(addData);
    }

    public CustomerInfo getCustomerId(long id) {
        return customerInfoRepository.findById(id).orElseThrow();
    }
}
