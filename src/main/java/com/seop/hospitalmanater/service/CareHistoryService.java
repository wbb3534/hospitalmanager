package com.seop.hospitalmanater.service;

import com.seop.hospitalmanater.entity.CareHistory;
import com.seop.hospitalmanater.entity.CustomerInfo;
import com.seop.hospitalmanater.model.CareHistoryItem;
import com.seop.hospitalmanater.model.CareHistoryRequest;
import com.seop.hospitalmanater.model.CareHistoryResponse;
import com.seop.hospitalmanater.model.MedicalCorporationItem;
import com.seop.hospitalmanater.repository.CareHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CareHistoryService {
    private final CareHistoryRepository careHistoryRepository;

    public void setCareHistory(CustomerInfo customerInfo, CareHistoryRequest request) {
        CareHistory addData = new CareHistory.CareHistoryBuilder(customerInfo, request).build();

        careHistoryRepository.save(addData);
    }

    public List<CareHistoryItem> getHistoryByDate(LocalDate searchDate) {
        List<CareHistory> originList = careHistoryRepository.findAllByCureDateOrderByHistoryIdDesc(searchDate);
        List<CareHistoryItem> result = new LinkedList<>();

        /*for (CareHistory item : originList) {
            CareHistoryItem addItem = new CareHistoryItem.CareHistoryItemBuilder(item).build();
            result.add(addItem);
        }*/

        originList.forEach(item -> result.add(new CareHistoryItem.CareHistoryItemBuilder(item).build()));

        return result;
    }

    public List<MedicalCorporationItem> getMedicalCorporation(LocalDate searchDate) {
        List<CareHistory> originList = careHistoryRepository.findAllByIsSalaryAndCureDateAndIsCalculateOrderByHistoryIdDesc(true, searchDate, true);
        List<MedicalCorporationItem> result = new LinkedList<>();

       /* for (CareHistory item : originList) {
            MedicalCorporationItem addItem = new MedicalCorporationItem.MedicalCorporationItemBuilder(item).build();

            result.add(addItem);
        }*/
        originList.forEach(item -> result.add(new MedicalCorporationItem.MedicalCorporationItemBuilder(item).build()));

        return result;
    }

    public CareHistoryResponse getCareHistory(long id) {
        CareHistory originData = careHistoryRepository.findById(id).orElseThrow();

        return new CareHistoryResponse.CareHistoryResponseBuilder(originData).build();
    }

    public void putCalculate(long id) {
        CareHistory originDate = careHistoryRepository.findById(id).orElseThrow();
        originDate.putCompletePay();
        //originDate.setIsCalculate(true);

        careHistoryRepository.save(originDate);
    }
}
